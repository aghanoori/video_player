let myplayers = document.querySelectorAll(".myplayer");
myplayers.forEach(function(myplayer){

    let controls = myplayer.querySelector(".myplayer__controls");
    let media = myplayer.querySelector("video");
    let play = controls.querySelector(".play");
    let rwd = controls.querySelector(".rewind");
    let fwd = controls.querySelector(".forward");
    let timer = controls.querySelector(".timer");
    let current_time = timer.querySelector(".currentTime");
    let video_time = timer.querySelector(".videoTime");
    let input = controls.querySelector(".controls__progressbar-current");
    let volume = controls.querySelector(".volume");
    let volume_icon = volume.querySelector("i");
    let volume_progress = volume.querySelector(".volume__progress");
    let input_volume = volume_progress.querySelector("input");
    let fullscreen_icon = controls.querySelector(".fullscreen .icon");
    console.log(fullscreen_icon)
    media.volume =0.5;
    
    play.addEventListener("click",function(){
        video_time.textContent =gettime(media.duration);
       if(media.paused){
            toggleplayicon()
            media.play();
       }else{
            toggleplayicon()
            media.pause();
       }
    });
    
    rwd.addEventListener("click" , function(){
        media.currentTime = media.currentTime - 5;
    });
    
    fwd.addEventListener("click" , function(){
        media.currentTime = media.currentTime + 5;
    });
    
    media.addEventListener("timeupdate" , function(){
        current_time.textContent =  gettime(media.currentTime);

        let barlength = (media.currentTime / media.duration)*100;
        input.style.background = `linear-gradient(90deg, rgba(230,126,34,1) ${barlength}%, #e1e1e1 0%)`;
    });

    input.addEventListener("input", function(){
        media.currentTime =( this.value /100)*media.duration;
    });

    volume_icon.addEventListener("click" , function() {
        volume_progress.classList.toggle("active");
    });
    
    input_volume.addEventListener("input" , function() {
        media.valume = this.value /100;
        input_volume.style.background = `linear-gradient(90deg, rgba(230,126,34,1) ${this.value}%, #e1e1e1 0%)`;
    });

    fullscreen_icon.addEventListener("click" , function() {
        if (!document.fullscreenElement) {
            myplayer.requestFullscreen();
        } else {
          if (document.exitFullscreen) {
            document.exitFullscreen(); 
          }
        }
    })


    function toggleplayicon() {
        let icon = play.querySelector("i");
        icon.classList.toggle("ion-md-play");
        icon.classList.toggle("ion-md-pause");
    };
 
    function gettime(time) {
        let minutes =Math.floor(time /60) ;
        let seconds = Math.floor(time- (minutes *60));
        if(minutes < 10 ) {
            minutes = "0" + minutes;
        }else{
            minutes;
        }
        if(seconds < 10 ) {
            seconds = "0" + seconds;
        }else{
            seconds;
        }
        return minutes + ":" + seconds;
    };
});
